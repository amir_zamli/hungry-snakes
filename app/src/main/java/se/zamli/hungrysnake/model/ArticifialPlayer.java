package se.zamli.hungrysnake.model;

import android.util.SparseArray;


/**
 * Created by Zamlii on 2015-08-28.
 *
 * The thought is that:
 * You provide the field for the articifial player and whick snake it plays as,
 * it will then try all possible scenarios and saves the best ones and randomly choose one from them.
 *
 * This class is dependen on SnakeEngine class.
 */
public class ArticifialPlayer {

    /**
     * adds vectors to array if vectors are not null
     * @param vectorArrayList
     * @param direction
     * @param vector
     * @param steps amount of steps that has been tested
     */
    private static void addIfNotNull(SparseArray<Integer> vectorArrayList, int direction, Vector vector, int steps){
        if(vector != null){
            Integer value = vectorArrayList.get(direction);
            value += steps;
            vectorArrayList.remove(direction);
            vectorArrayList.put(direction, value);
        }
    }

    /**
     * compute vectors in all directions with given amount of steps
     */
    private static void computeNewVectorPositions(SnakeEngine snakeEngine, SparseArray<Integer> scenarios, Vector currentVectorPos, int steps){
        addIfNotNull(scenarios, SnakeEngine.MOVE_LEFT , snakeEngine.getNewVectorPosition(currentVectorPos, SnakeEngine.MOVE_LEFT    , steps, true), steps);
        addIfNotNull(scenarios, SnakeEngine.MOVE_RIGHT, snakeEngine.getNewVectorPosition(currentVectorPos, SnakeEngine.MOVE_RIGHT   , steps, true), steps);
        addIfNotNull(scenarios, SnakeEngine.MOVE_UP   , snakeEngine.getNewVectorPosition(currentVectorPos, SnakeEngine.MOVE_UP      , steps, true), steps);
        addIfNotNull(scenarios, SnakeEngine.MOVE_DOWN , snakeEngine.getNewVectorPosition(currentVectorPos, SnakeEngine.MOVE_DOWN    , steps, true), steps);
    }


    /**
     * computes the best move possible; hopefully
     * @param snakeEngine
     * @param snake
     * @return the direction in which the snake should move.
     */
    public static int computeMove(SnakeEngine snakeEngine, Snake snake){
        //starts of at 3, if no path works with 3 steps then lower and find the best for 2 steps, etc...
        int steps = 1;

        //Create steps in all directions
        Vector currentVectorPos = snake.getSnakeCells().getLast();

        //Collisions will return false
        //Order is, left, right, up and down
        SparseArray<Integer> scenarios = new SparseArray<>();
        scenarios.put(SnakeEngine.MOVE_DOWN, 0);
        scenarios.put(SnakeEngine.MOVE_UP, 0);
        scenarios.put(SnakeEngine.MOVE_LEFT, 0);
        scenarios.put(SnakeEngine.MOVE_RIGHT, 0);
        while(steps <= 3){
            computeNewVectorPositions(snakeEngine, scenarios, currentVectorPos, steps);
            steps++;
        }


        int max = 0;
        for(int i = 0; i < scenarios.size(); i++) {
            int key = scenarios.keyAt(i);
            // get the object by the key.
            int value = scenarios.get(key);
            if(value > max){
                max = value;
            }
        }
        int index = scenarios.indexOfValue(max);
        return scenarios.keyAt(index);
    }


}
