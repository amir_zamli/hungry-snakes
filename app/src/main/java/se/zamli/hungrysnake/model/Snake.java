package se.zamli.hungrysnake.model;

import java.util.LinkedList;


/**
 * Created by Zamlii on 2015-08-25.
 */
public class Snake{
    private Boolean computer;
    private final String name;
    private final int resColorId;
    private final Vector startingPosition;
    private int lastSelectedDirection;

    private final LinkedList<Vector> snakeCells;

    Snake(String name, Boolean computer, int resColorId, Vector position){
        this.name = name;
        this.computer = computer;
        this.resColorId = resColorId;

        snakeCells = new LinkedList<>();
        startingPosition = position;
        resetSnake();
    }

    public void growSnake(Vector vector){
        //snakeCells.pop();
        snakeCells.add(vector);
    }

    private void resetSnake(){
        snakeCells.clear();
        snakeCells.add(startingPosition);
    }

    public void selectDirection(int direction){
        lastSelectedDirection = direction;
    }

    public int getLastSelectedDirection(){
        return lastSelectedDirection;
    }

    public String getName(){
        return name;
    }

    public LinkedList<Vector> getSnakeCells(){
        return snakeCells;
    }

    public int getResColorId(){
        return resColorId;
    }


    public void setComputer(){
        this.computer = true;
    }
    public Boolean isComputer(){
        return computer;
    }

}
