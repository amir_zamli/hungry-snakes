package se.zamli.hungrysnake.model;

import android.util.SparseArray;

import java.util.ArrayList;
import java.util.Random;

import se.zamli.hungrysnake.R;
import se.zamli.hungrysnake.view.GridViewCellAdapter;


/**
 * Class for handling snakes
 *
 * Created by Zamlii on 2015-08-25.
 */
public class SnakeEngine{
    private final int MIN_FOOD = 1;
    private final int MAX_FOOD = 3;

    public final static int MOVE_DOWN = 0;
    public final static int MOVE_UP = 1;
    public final static int MOVE_RIGHT = 2;
    public final static int MOVE_LEFT = 3;

    private OnSnakeEngineEventListener onSnakeEngineEventListener;

    private final GridViewCellAdapter gridViewCellAdapter;

    //Maps color to color names
    private final SparseArray<String> colorMap;

    private final ArrayList<Snake> snakes;
    private Snake nextSnake;

    //the cell where the collision occurred
    private Vector loosingCell;

    private final Random rand;

    public interface OnSnakeEngineEventListener{
        void onEvent(String event);
    }

    public void setEventListener(OnSnakeEngineEventListener onSnakeEngineEventListener){
        this.onSnakeEngineEventListener = onSnakeEngineEventListener;
    }

    public SnakeEngine(GridViewCellAdapter gridViewCellAdapter){
        snakes = new ArrayList<>();
        this.gridViewCellAdapter = gridViewCellAdapter;

        rand = new Random();
        colorMap = new SparseArray<>();
        colorMap.put(R.color.cyan_500, "Blue");
        colorMap.put(R.color.red_500, "Red");
        colorMap.put(R.color.green_500, "Green");
        colorMap.put(R.color.brown_500, "Brown");
        colorMap.put(R.color.yellow_500, "Yellow");
        colorMap.put(R.color.purple_500, "Purple");
        colorMap.put(R.mipmap.ic_launcher_konstantin, "SOZ");

    }


    /**
     *
     */
    public void reset(){
        snakes.clear();
        gridViewCellAdapter.clearField();
        nextSnake = null;
        loosingCell = null;
    }

    public int getResColorIdFromName(String name){
        for(int i = 0; i < colorMap.size(); i++) {
            int key = colorMap.keyAt(i);
            // get the object by the key.
            String str = colorMap.get(key).toString();
            String search = name.toString();
            if(str.equalsIgnoreCase(search)){
                return key;
            }
        }
        return GridViewCellAdapter.DEFAULT_COLOR;
    }


    public void addSnake(String name){
        Vector pos = gridViewCellAdapter.getVectorFromPosition(randInt(0, gridViewCellAdapter.getCount() - 1));
        int loopLimit = 0;
        while(gridViewCellAdapter.getCellColor(pos) != GridViewCellAdapter.DEFAULT_COLOR){
            pos = gridViewCellAdapter.getVectorFromPosition(randInt(0, gridViewCellAdapter.getCount() - 1));
            if(loopLimit > gridViewCellAdapter.getCount())
                break;
            else
                loopLimit++;
        }

        int index = colorMap.indexOfValue(name);
        int resColorId = colorMap.keyAt(index);
        Snake snake = new Snake(name, false, resColorId, pos);

        //SnakeEngine should observe the snake
        snakes.add(snake);
        nextSnake = snake;//latest added snakes turn, by default

        gridViewCellAdapter.setCellColor(pos, snake.getResColorId());
        onSnakeEngineEventListener.onEvent("next_player," + nextSnake.getName());
        gridViewCellAdapter.drawField();
    }


    public Snake getNextSnake(){
        return nextSnake;
    }

    /**
     * When resetting snakes are cleared, but sometimes old references of snakes are saved
     * we check these to make sure that we dont use them
     * @param snake
     * @return
     */
    public boolean checkSnakeAlive(Snake snake){
        return snakes.contains(snake);
    }

    /**
     * row and column vary between 0 and numcollumn-1
     *
     * @param vectorPosition
     * @param direction
     * @return new position or -1 if colliding with something
     */
    public Vector getNewVectorPosition(Vector vectorPosition, int direction, int steps, Boolean checkCollision){
        int row = vectorPosition.getRow();
        int column = vectorPosition.getColumn();

        switch(direction){
            case MOVE_DOWN:
                row+=steps;
                break;
            case MOVE_UP:
                row-=steps;
                break;
            case MOVE_RIGHT:
                column+=steps;
                break;
            case MOVE_LEFT:
                column-=steps;
                break;
        }

        if(row >= gridViewCellAdapter.getNumRows() || row < 0){
            return null;
        }
        if(column >= gridViewCellAdapter.getNumColumns() || column < 0){
            return null;
        }

        Vector newPosition = new Vector(column, row);
        if(checkCollision && collisionDetect(newPosition))
            return null;
        else
            return newPosition;
    }


    /**
     * Checks if any collision would occur if snake would move to this position
     * @param checkPosition
     * @return true if collision occurs.
     */
    private boolean collisionDetect(Vector checkPosition){
        int cellColor = gridViewCellAdapter.getCellColor(checkPosition);
        return cellColor != GridViewCellAdapter.DEFAULT_COLOR;
    }


    /**
     * Gets called after player selects direction ONLY!
     * update gridview with new cell colors
     * TODO optimize by only setting new colors.
     */
    public void drawFrame(Snake snake){
        if(snake.isComputer()){
            if(snake.getName().equals("Yellow")){
                //This AI is a little bit retarded...
                snake.selectDirection(ArticifialPlayer.computeMove(this, snake));
            }else{
                //This is the good AI
                snake.selectDirection(ArticifialPlayer2.computeMove(this, snake));
            }
        }

        if(!feedSnake(snake, randInt(MIN_FOOD, MAX_FOOD))){
            gridViewCellAdapter.clearField(R.color.black);

            String text = "";
            for(Snake snake1: snakes){
                for(Vector position: snake1.getSnakeCells()){
                    gridViewCellAdapter.setCellColor(position, snake1.getResColorId());
                }
                int length = snake1.getSnakeCells().size();
                text += " | "+snake1.getName()+": "+length;
            }

            onSnakeEngineEventListener.onEvent("lost," + colorMap.get(snake.getResColorId())+","+text);

            if(loosingCell != null){
                gridViewCellAdapter.setCellColor(loosingCell, R.mipmap.ic_launcher_amir);
            }

            gridViewCellAdapter.drawField();

        }

        gridViewCellAdapter.drawField();
    }


    //return false if snake collides with somethings..
    private boolean feedSnake(Snake snake, int amount){
        if(amount > 0){
            Vector newPosition = getNewVectorPosition(snake.getSnakeCells().getLast(), snake.getLastSelectedDirection(), 1, false);

            if(newPosition == null){
                return false;
            }else if(!gridViewCellAdapter.setCellColor(newPosition, snake.getResColorId())){
                loosingCell = newPosition;
                return false;
            }

            snake.growSnake(newPosition);
            return feedSnake(snake, amount-1);
        }else{
            //Here we set the next snakes turn
            int snakeIndex = snakes.indexOf(snake);
            if(snakeIndex >= snakes.size()-1){
                nextSnake = snakes.get(0);
            }else{
                nextSnake = snakes.get(snakeIndex+1);
            }
            onSnakeEngineEventListener.onEvent("next_player," + nextSnake.getName());
            return true;
        }


    }

    public int randInt(int min, int max){
        return rand.nextInt((max - min) + 1) + min;
    }


}
