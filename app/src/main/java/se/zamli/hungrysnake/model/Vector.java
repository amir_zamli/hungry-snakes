package se.zamli.hungrysnake.model;

/**
 * Created by Zamlii on 2015-08-31.
 *
 * each vector represents a cell in the grid.
 */
public class Vector {

    private final int column;
    private final int row;

    public Vector(int column, int row){
        this.column = column;
        this.row = row;

    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public Vector clone(){
        return new Vector(column, row);
    }
}
