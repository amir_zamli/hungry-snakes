package se.zamli.hungrysnake.model;

import android.util.Log;
import android.util.SparseArray;


/**
 * Created by Zamlii on 2015-08-28.
 *
 * The thought is that:
 * You provide the field for the articifial player and whick snake it plays as,
 * it will then try all possible scenarios and saves the best ones and randomly choose one from them.
 *
 * This class is dependen on SnakeEngine class.
 */
public class ArticifialPlayer2 {


    /**
     * Sets a specific bit of an int.
     *
     * @param bit the bit to set. The least significant bit is bit 0
     * @param target the integer where the bit will be set
     * @return the updated value of the target
     */
    private static int setBit(int bit, int target) {
        // Create mask
        int mask = 1 << bit;
        // Set bit
        return target | mask;
    }


    /**
     * adds vectors to array if vectors are not null
     * @param vectorArrayList
     * @param direction
     * @param vector
     * @param steps amount of steps that has been tested
     */
    private static void addIfNotNull(SparseArray<Integer> vectorArrayList, int direction, Vector vector, int steps){
        if(vector != null){
            Integer value = vectorArrayList.get(direction);

            //set bit at position steps-1 to 1.
            value = setBit(steps-1, value);

            vectorArrayList.remove(direction);
            vectorArrayList.put(direction, value);
        }
    }

    /**
     * compute vectors in all directions with given amount of steps
     */
    private static void computeNewVectorPositions(SnakeEngine snakeEngine, SparseArray<Integer> scenarios, Vector currentVectorPos, int steps){
        addIfNotNull(scenarios, SnakeEngine.MOVE_LEFT , snakeEngine.getNewVectorPosition(currentVectorPos, SnakeEngine.MOVE_LEFT    , steps, true), steps);
        addIfNotNull(scenarios, SnakeEngine.MOVE_RIGHT, snakeEngine.getNewVectorPosition(currentVectorPos, SnakeEngine.MOVE_RIGHT   , steps, true), steps);
        addIfNotNull(scenarios, SnakeEngine.MOVE_UP   , snakeEngine.getNewVectorPosition(currentVectorPos, SnakeEngine.MOVE_UP      , steps, true), steps);
        addIfNotNull(scenarios, SnakeEngine.MOVE_DOWN , snakeEngine.getNewVectorPosition(currentVectorPos, SnakeEngine.MOVE_DOWN    , steps, true), steps);
    }

    /** not needed anymore
    public static void removeBadScenarios(SparseArray<Integer> scenarios, int testBit){
        for(int i = 0; i < scenarios.size(); i++) {
            int key = scenarios.keyAt(i);
            // get the object by the key.
            int value = scenarios.get(key);
            if((value & testBit) == 0){
                scenarios.remove(key);
            }
        }
    }*/

    /**
     *
     * @param scenarios
     * @param testBit
     * @return
     */
    private static SparseArray<Integer> filterGoodScenarios(SparseArray<Integer> scenarios, int testBit){
        SparseArray<Integer> filteredScenarios = new SparseArray<>();

        for(int i = 0; i < scenarios.size(); i++) {
            int key = scenarios.keyAt(i);
            // get the object by the key.
            int value = scenarios.get(key);
            if((value & 1) > 0){
                filteredScenarios.put(key, value>>1);
                Log.i("AI", "Catching: "+ Integer.toBinaryString(value)+", with testbit:"+Integer.toBinaryString(testBit));
            }
        }

        if(filteredScenarios.size() != 0)
            return filteredScenarios;
        else
            return null;
    }

    /**
     * computes the best move possible; hopefully
     * @param snakeEngine
     * @param snake
     * @return the direction in which the snake should move.
     */
    public static int computeMove(SnakeEngine snakeEngine, Snake snake){
        //starts of at 3, if no path works with 3 steps then lower and find the best for 2 steps, etc...
        int steps = 1;
        int maxTestSteps = 7;
        int binaryNumber = (int)Math.pow(2, maxTestSteps);

        //Create steps in all directions
        Vector currentVectorPos = snake.getSnakeCells().getLast();

        //Collisions will return false
        //Order is, left, right, up and down
        SparseArray<Integer> scenarios = new SparseArray<>();
        scenarios.put(SnakeEngine.MOVE_DOWN, 0);
        scenarios.put(SnakeEngine.MOVE_UP, 0);
        scenarios.put(SnakeEngine.MOVE_LEFT, 0);
        scenarios.put(SnakeEngine.MOVE_RIGHT, 0);
        while(steps <= maxTestSteps){
            computeNewVectorPositions(snakeEngine, scenarios, currentVectorPos, steps);
            steps++;
        }


        //First we assign the last move a random direction in case nothing will be given it;
        int lastMove = 0;
        for(int testBits = 0; testBits < binaryNumber && scenarios != null; testBits = (testBits<<1)+1){
            //now the lastMove will be the first in the scenarios available
            lastMove = scenarios.keyAt(0);
            scenarios = filterGoodScenarios(scenarios, testBits);
        }
        //If there is any scenario left, we want it
        if(scenarios != null && scenarios.size() != 0){
            lastMove = scenarios.keyAt(0);
        }

        return lastMove;
    }



}
