package se.zamli.hungrysnake.view;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.TextView;


import se.zamli.hungrysnake.R;
import se.zamli.hungrysnake.model.Snake;
import se.zamli.hungrysnake.model.SnakeEngine;

/**
 * Created by Zamlii on 2015-08-25.
 *
 * activity is like a window containing all the views and creating a context.
 */
public class MainActivity extends AppCompatActivity implements
        View.OnClickListener, //reacting on view clicks
        SnakeEngine.OnSnakeEngineEventListener{ //SnakeEngine tells when events happen

    private Toolbar toolbar;

    private GridView gridView; //Gridview containing the squares
    private GridViewCellAdapter gridViewCellAdapter;

    private SnakeEngine snakeEngine;

    private TextView statusText;
    private View turnIndicator;


    //A little easter egg helper
    private int easterEgg = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Binding the views
        toolbar = (Toolbar)findViewById(R.id.toolbar);

        gridView = (GridView)findViewById(R.id.fragment_main_activity_main_gridview_worm_field);

        statusText = (TextView)findViewById(R.id.activity_main_textview_status);
        turnIndicator = (View)findViewById(R.id.activity_main_iv_turn_indicator);

        initToolbarView();

        //After binding the views we can set this class as listener for clicks.
        setOnClickListeners();

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int padding = 11;
        int numColumns = 12;
        int numRows = numColumns;

        int cellSize = metrics.widthPixels;
        cellSize -= padding*(numColumns +1);
        cellSize /= numColumns;

        gridViewCellAdapter = new GridViewCellAdapter(this, numColumns, cellSize);
        //Setting upp the grid graphics for the snakes to be displayed on.
        gridView.setAdapter(gridViewCellAdapter);
        gridView.setNumColumns(numColumns);

        //when grid cell is clicked we want to be able to create "walls" with easter egg.
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (easterEgg >= 3) {
                    gridViewCellAdapter.setCellColor(gridViewCellAdapter.getVectorFromPosition(position), R.color.black);
                    gridViewCellAdapter.drawField();
                }
            }
        });

        snakeEngine = new SnakeEngine(gridViewCellAdapter);
        initSnakeEngine();
    }


    /**
     * inits toolbar view.
     */
    public void initToolbarView(){
        //Sets bar color on Lollipop, just visual niceties
        //for some fucked up reason one cant set this in xml... a bug?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.bored_primary));
        }

        //Sets the toolbar from view and removes the title so that we can have the manually added one, in xml.
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if(ab != null) {
            ab.setDisplayShowTitleEnabled(false);
        }
    }

    /**
     * Sets so that all views send their clicks to this class
     */
    private void setOnClickListeners(){
        final GridLayout gridLayout = (GridLayout)findViewById(R.id.activity_main_buttons_container);
        for(int i = 0; i < gridLayout.getChildCount(); i++){
            gridLayout.getChildAt(i).setOnClickListener(MainActivity.this);
        }

        final GridLayout gridLayout2 = (GridLayout)findViewById(R.id.activity_main_buttons_color_container);
        for(int i = 0; i < gridLayout2.getChildCount(); i++){
            gridLayout2.getChildAt(i).setOnClickListener(MainActivity.this);
        }

        findViewById(R.id.activity_main_button_reset).setOnClickListener(this);
        findViewById(R.id.activity_main_button_ai).setOnClickListener(this);

        turnIndicator.setOnClickListener(this);//just for the easter egg :)
    }
    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        Snake snake = snakeEngine.getNextSnake();

        int viewId = v.getId();
        if(snake != null){
            switch (viewId) {
                case R.id.activity_main_button_up:
                    snake.selectDirection(SnakeEngine.MOVE_UP);
                    snakeEngine.drawFrame(snake);
                    break;
                case R.id.activity_main_button_down:
                    snake.selectDirection(SnakeEngine.MOVE_DOWN);
                    snakeEngine.drawFrame(snake);
                    break;
                case R.id.activity_main_button_left:
                    snake.selectDirection(SnakeEngine.MOVE_LEFT);
                    snakeEngine.drawFrame(snake);
                    break;
                case R.id.activity_main_button_right:
                    snake.selectDirection(SnakeEngine.MOVE_RIGHT);
                    snakeEngine.drawFrame(snake);
                    break;
                case R.id.activity_main_button_ai:
                    if(!snake.isComputer()){
                        snake.setComputer();
                        onEvent("next_player," + snake.getName());
                    }
                    break;
            }
        }

        switch (viewId){
            case R.id.activity_main_button_reset:
                onReset();
                break;
            case R.id.activity_main_button_blue:
                snakeEngine.addSnake("Blue");
                break;
            case R.id.activity_main_button_red:
                snakeEngine.addSnake("Red");
                break;
            case R.id.activity_main_button_yellow:
                snakeEngine.addSnake("Yellow");
                break;
            case R.id.activity_main_button_green:
                snakeEngine.addSnake("Green");
                break;
            case R.id.activity_main_button_brown:
                snakeEngine.addSnake("Brown");
                break;
            case R.id.activity_main_button_purple:
                snakeEngine.addSnake("Purple");
                break;
            case R.id.activity_main_iv_turn_indicator:
                if(easterEgg++ >= 5){
                    snakeEngine.addSnake("SOZ");
                    easterEgg = 0;
                }
                break;
        }
    }


    private void initSnakeEngine(){
        snakeEngine.setEventListener(this);
    }

    private void onReset(){
        snakeEngine.reset();
        gridViewCellAdapter.drawField();
        turnIndicator.setBackgroundResource(R.color.white);
        statusText.setText("");
        easterEgg = 0;
    }

    @Override
    public void onEvent(String event) {
        String[] strings = event.split(",");

        //For some reason i have to convert string to string...
        String type  = strings[0].toString();
        String value = strings[1].toString();
        switch (type) {
            case "next_player":
                statusText.setText(value + " snakes turn now");
                int resColorId = snakeEngine.getResColorIdFromName(value);
                turnIndicator.setBackgroundResource(resColorId);

                final Snake nextSnake = snakeEngine.getNextSnake();
                postDelayedSnakeHandler(nextSnake);

                break;
            case "lost":
                statusText.setText(strings[1]+" snake lost! "+strings[2]);
                turnIndicator.setBackgroundResource(R.color.white);
                break;
        }
    }

    /**
     * gives computer a turn but will a 130ms delay.
     * @param snake
     */
    private void postDelayedSnakeHandler(final Snake snake){
        if(snake.isComputer()){
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                //Delay computer for visual impression
                public void run() {
                    if (snakeEngine.checkSnakeAlive(snake)) {
                        //if snake is computer send it to drawframe method and it will figure shit out
                        snakeEngine.drawFrame(snake);
                    }
                }
            }, 130);
        }
    }
}
