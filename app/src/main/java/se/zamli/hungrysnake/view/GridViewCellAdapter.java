package se.zamli.hungrysnake.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collections;

import se.zamli.hungrysnake.R;
import se.zamli.hungrysnake.model.Vector;

/**
 * Created by Zamlii on 2015-08-25.
 */
public class GridViewCellAdapter extends BaseAdapter{
    public static final int DEFAULT_COLOR = R.color.white;

    private int numColumns;
    private int numRows;
    private final Context mContext;


    private ArrayList<Integer> cellColorIds;

    private int cellSize;


    public GridViewCellAdapter(Context c, int numColumnsAndRows, int cellSize) {
        mContext = c;

        this.numColumns = numColumnsAndRows;
        this.numRows = numColumnsAndRows;
        this.cellSize = cellSize;

        clearField();
    }

    public int getNumRows(){
        return numRows;
    }

    public int getNumColumns(){
        return numColumns;
    }

    public int getCount() {
        return cellColorIds.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Integer getItem(int position) {
        return cellColorIds.get(position);
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    public int getCellColor(Vector vector){
        int position = getPositionFromVector(vector);
        return cellColorIds.get(position);
    }

    /**
     * returns false if cell has other color than Default Color
     *
     * @param vector
     * @param resId
     * @return true if cell is default color and no obstacle there.
     */
    public Boolean setCellColor(Vector vector, int resId){
        int position = getPositionFromVector(vector);

        // save old color
        int oldColor = cellColorIds.get(position);

        //set new color
        cellColorIds.set(position, resId);

        return oldColor == DEFAULT_COLOR;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        GridViewItem imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new GridViewItem(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(cellSize, cellSize));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (GridViewItem)convertView;
        }

        imageView.setImageResource(cellColorIds.get(position));
        return imageView;
    }


    /**
     * should only be used with new games(resetting or just opened app)
     */
    public void clearField(){
        clearField(DEFAULT_COLOR);
    }

    public void clearField(int resColorId){
        int numCells = numColumns *numRows;

        //Fills the array list with in our case 64 of resource id for default color
        cellColorIds =  new ArrayList<>(Collections.nCopies(numCells, resColorId));
    }

    public void drawField(){
        notifyDataSetChanged();
    }


    ////////////////////////cell coordination////////////////////////

    private int getRowFromPosition(int position){
        return (int) Math.floor((position) / (float) numColumns);
    }


    //ugh
    private int getCollumnFromPosition(int position){
        int row = getRowFromPosition(position);
        return position-(row * numColumns);
    }


    //After a whole lot of thinking...
    private int getPositionFromVector(Vector vector){
        return vector.getRow()* numColumns + vector.getColumn();
    }


    //After a whole lot of thinking...
    public Vector getVectorFromPosition(int position){
        return new Vector(getCollumnFromPosition(position), getRowFromPosition(position));
    }


}