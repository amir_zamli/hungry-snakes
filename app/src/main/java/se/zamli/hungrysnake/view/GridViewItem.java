package se.zamli.hungrysnake.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Copied from http://stackoverflow.com/questions/24416847/how-to-force-gridview-to-generate-square-cells-in-android
 *
 * Helps with getting the cells square in size.
 */
public class GridViewItem extends ImageView {

    public GridViewItem(Context context) {
        super(context);
    }

    public GridViewItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GridViewItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // This is the key that will make the height equivalent to its width
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
